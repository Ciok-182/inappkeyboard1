//
//  NumericKeyboard.swift
//  inAppKeyboard
//
//  Created by Jorge Encinas on 7/25/18.
//  Copyright © 2018 Jorge Encinas. All rights reserved.
//

import UIKit


// private consts
private let pressedBackgroundImage = UIImage(named: "Preseed")!
private let normalBackgroundImage = UIImage(named: "Unpressed")!
private let numericKeyboardNormalImage = UIImage(named: "numericKeyBackground")!
private let numericKeyboardPressedImage = UIImage(named: "pressedNumericKeyBackground")!



@objc protocol NumericKeyboardDelegate {
    func numericKeyPressed(key: Int)
    func numericBackspacePressed()
    func numericSymbolPressed(symbol: String)
}

class NumericKeyboard: UIView {
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var button8: UIButton!
    @IBOutlet weak var button9: UIButton!
    @IBOutlet weak var button0: UIButton!
    @IBOutlet weak var buttonPoint: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    
    var allButtons: [UIButton] { return [button0, button1, button2, button3, button4, button5, button6,
        button7, button8, button9, buttonPoint, buttonBack] }
    var Buttons123: [UIButton] { return [button1, button2, button3] }
    var Buttons456: [UIButton] { return [button4, button5, button6] }
    var Buttons789: [UIButton] { return [button7, button8, button9] }
    
    
    // data
    weak var delegado: NumericKeyboardDelegate?
    
    // MARK: - Initialization and lifecycle.
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeKeyboard()
        addLognpress()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initializeKeyboard()
        addLognpress()
    }
    
    func addLognpress(){
        //        longPress.minimumPressDuration = 0.5
        //        longPress.numberOfTouchesRequired = 1
        //        longPress.allowableMovement = 0.1
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        
        buttonBack.addGestureRecognizer(longGesture)
    }
    
    func initializeKeyboard() {
        // set view
        let xibFileName = "MyNumericKeyboard"
        let view = Bundle.main.loadNibNamed(xibFileName, owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
        updateButtonsAppearance()
    }
    
    // MARK: - Changes in appearance
    fileprivate func updateButtonsAppearance() {
        
       
        for button in allButtons {
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitleColor(UIColor.red, for: .highlighted)
        }
        /*
        for button in Buttons123 {
            button.setTitleColor(UIColor.blue, for: .normal)
            button.setTitleColor(UIColor.red, for: .highlighted)
            button.setBackgroundImage(numericKeyboardNormalImage, for: .normal)
            button.setBackgroundImage(numericKeyboardPressedImage, for: .highlighted)
        }
        
        for button in Buttons456 {
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitleColor(UIColor.red, for: .highlighted)
            button.setBackgroundImage(normalBackgroundImage, for: .normal)
            button.setBackgroundImage(pressedBackgroundImage, for: .highlighted)
        }
        
        for button in Buttons789 {
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitleColor(UIColor.red, for: .highlighted)
        }*/
    }
  
    
    @objc func handleLongPress(_ gestureRecognizer: UIGestureRecognizer) {
        print("\n\nhandleLongPress\n\n")
        self.delegado?.numericBackspacePressed()
    }
    
    // MARK: - Button actions
    @IBAction func numericButtonPressed(_ sender: UIButton) {
        //sender.setTitleColor(UIColor.yellow, for: .highlighted)
        print("IBAction numericButtonPressed ")
        self.delegado?.numericKeyPressed(key: sender.tag)
    }
    
    @IBAction func pointButtomPressed(_ sender: UIButton) {
        print("IBAction pointButtomPressed ")
        
        if let symbol = sender.titleLabel?.text, symbol.count > 0 {
            self.delegado?.numericSymbolPressed(symbol: symbol)
        }
    }
    
    @IBAction func backButtomPressed(_ sender: Any) {
        
        print("IBAction backButtomPressed ")
        self.delegado?.numericBackspacePressed()
    }
}

private var numericKeyboardDelegate: NumericKeyboardDelegate? = nil

extension UITextField: NumericKeyboardDelegate {
    
    // MARK: - Public methods to set or unset this uitextfield as NumericKeyboard.
    
    func setAsNumericKeyboard(delegate: NumericKeyboardDelegate?) {
        let numericKeyboard = NumericKeyboard(frame: CGRect(x: 0, y: 0, width: 0, height: numericKeyboardRecommendedHeight))
        self.inputView = numericKeyboard
        numericKeyboardDelegate = delegate
        numericKeyboard.delegado = self
    }
    
    func unsetAsNumericKeyboard() {
        if let numericKeyboard = self.inputView as? NumericKeyboard {
            numericKeyboard.delegado = nil
        }
        self.inputView = nil
        numericKeyboardDelegate = nil
    }
    
    
    // MARK: - NumericKeyboardDelegate methods
    
    internal func numericKeyPressed(key: Int) {
        self.text?.append("\(key)")
        numericKeyboardDelegate?.numericKeyPressed(key: key)
    }
    
    internal func numericBackspacePressed() {
        if var text = self.text, text.count > 0 {
            _ = text.remove(at: text.index(before: text.endIndex))
            self.text = text
        }
        numericKeyboardDelegate?.numericBackspacePressed()
    }
    
    internal func numericSymbolPressed(symbol: String) {
        self.text?.append(symbol)
        numericKeyboardDelegate?.numericSymbolPressed(symbol: symbol)
    }
}
