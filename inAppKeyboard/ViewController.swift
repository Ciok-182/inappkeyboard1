//
//  ViewController.swift
//  inAppKeyboard
//
//  Created by Jorge Encinas on 7/25/18.
//  Copyright © 2018 Jorge Encinas. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NumericKeyboardDelegate {
   
    @IBOutlet weak var txtFielCustom: UITextField!
    @IBOutlet weak var txtFieldNormal: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewWillAppear(true)
        txtFielCustom.setAsNumericKeyboard(delegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numericKeyPressed(key: Int) {
        print("ViewController Numeric key \(key) pressed!")
    }
    
    func numericBackspacePressed() {
        print("ViewController Backspace pressed!")
    }
    
    func numericSymbolPressed(symbol: String) {
        print("ViewController Symbol \(symbol) pressed!")
    }

}

